# 2. Faire évoluer les applications avec des déploiements : Mise à l'échèle
------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______


# Introduction à la mise à l'échelle des applications avec les déploiements

Dans cette leçon, nous allons parler de la mise à l'échelle des applications avec les déploiements. Voici un aperçu rapide des sujets que nous allons aborder :

1. Qu'est-ce que la mise à l'échelle ?
2. La mise à l'échelle en relation avec les déploiements Kubernetes
3. Le processus de mise à l'échelle d'un déploiement
4. Une démonstration pratique

# Qu'est-ce que la mise à l'échelle ?

La **mise à l'échelle** consiste à allouer plus ou moins de ressources à une application pour répondre à des besoins changeants. En gros, cela signifie ajuster la quantité de ressources consacrées à une application particulière. Les déploiements Kubernetes sont très utiles pour la mise à l'échelle horizontale, qui consiste à modifier le nombre de conteneurs exécutant une application. Nous allouons plus de ressources à une application en créant plus de conteneurs qui l'exécutent, ou moins de ressources en diminuant le nombre de conteneurs exécutant cette application.

# La mise à l'échelle avec les déploiements Kubernetes

Le paramètre de réplicas d'un déploiement détermine combien de conteneurs ou de pods sont exécutés selon notre état désiré. Si nous modifions ce nombre de réplicas, des pods répliqués seront créés ou supprimés pour satisfaire ce nouveau nombre. Cela signifie que nous pouvons augmenter l'échelle en créant plus de pods en augmentant le nombre de réplicas, et nous pouvons réduire l'échelle en supprimant des pods supplémentaires en diminuant le nombre de réplicas.

# Processus de mise à l'échelle d'un déploiement

Il existe plusieurs façons de mettre à l'échelle un déploiement :

1. **Modifier le fichier YAML du déploiement** : Nous pouvons changer le nombre de répliques dans notre descripteur YAML. Par exemple, nous pouvons utiliser `kubectl apply` pour appliquer un fichier YAML avec un nombre de répliques modifié, ou utiliser `kubectl edit` pour modifier le descripteur YAML du déploiement en place.

```yaml
...
spec:
  replicas: 5
...
```

2. **Utiliser la commande `kubectl scale`** : C'est une commande spéciale que nous pouvons utiliser spécifiquement pour mettre à l'échelle les déploiements. 

La syntaxe est : 

```yaml
kubectl scale deployment.v1.apps/<nom_du_déploiement> --replicas=5
```

3. **Editer en direct le deployment** : 

```bash
kubectl edit deployment my-deployment
```

Une fois le descripteur YAML affichier, nous pouvons nous rendre sous la section **spec:** et modifier le champs **replicas:**

```yaml
...
spec:
  progressDeadlineSeconds: 600
  revisionHistoryLimit: 10
  ...
```


# Démonstration pratique

Voyons maintenant comment mettre à l'échelle un déploiement avec une démonstration pratique.

1. **Connexion au serveur de contrôle**

   Connectez-vous à votre serveur de contrôle Kubernetes :

```sh
ssh cloud_user@<PUBLIC_IP_ADDRESS>
```

2. **Modification du fichier de déploiement**

   Si vous avez suinano la leçon précédente, vous devriez avoir un fichier appelé `my-deployment.yml`. Ouvrez ce fichier pour le modifier :

```sh
nano my-deployment.yml
```

   Vous devriez voir votre descripteur de déploiement. Changez le nombre de répliques de 3 à 5 :

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
    name: my-deployment
spec:
    replicas: 5
    selector:
    matchLabels:
        app: my-deployment
    template:
    metadata:
        labels:
        app: my-deployment
    spec:
        containers:
        - name: nginx
        image: nginx:1.19.1
        ports:
        - containerPort: 80
```

   Enregistrez et quittez le fichier en appuyant sur `Échap` suinano de `:wq`.

3. **Application des modifications**

   Utilisez `kubectl apply` pour appliquer les modifications :

```sh
kubectl apply -f my-deployment.yml
```

   Vous verrez un message indiquant que le déploiement a été configuré.

4. **Vérification de la mise à l'échelle**

   Vérifiez le déploiement pour vous assurer qu'il a bien été mis à l'échelle :

```sh
kubectl get deployments
```

   Vous verrez maintenant que vous avez 5 répliques prêtes.

   Vérifiez les pods :

```sh
kubectl get pods
```

   Vous verrez les 5 répliques de pods.

5. **Mise à l'échelle avec `kubectl scale`**

   Vous pouvez également utiliser `kubectl scale` pour réduire l'échelle à 3 répliques :

```sh
kubectl scale deployment.v1.apps/my-deployment --replicas=3
```

   Vérifiez à nouveau les déploiements et les pods pour voir que l'échelle a été réduite :

```sh
kubectl get deployments
kubectl get pods
```

   Vous verrez que les deux pods supplémentaires ont été terminés et que vous avez maintenant 3 répliques de pods.

# Conclusion

Pour récapituler, nous avons répondu à la question : qu'est-ce que la mise à l'échelle ? Nous avons parlé de la mise à l'échelle en relation avec les déploiements Kubernetes, expliqué comment mettre à l'échelle un déploiement, et réalisé une démonstration pratique de ce processus dans notre cluster. C'est tout pour cette leçon. À la prochaine !


# Reférences

https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#scaling-a-deployment